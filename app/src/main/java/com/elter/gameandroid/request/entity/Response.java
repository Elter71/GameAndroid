package com.elter.gameandroid.request.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by elter on 08.10.17.
 */

public class Response {
    @SerializedName("type")
    int type;

    @SerializedName("data")
    String data;


    public Response(int type, String data) {
        this.type = type;
        this.data = data;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
