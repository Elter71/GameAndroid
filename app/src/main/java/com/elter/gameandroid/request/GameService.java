package com.elter.gameandroid.request;

import com.elter.gameandroid.request.entity.Response;
import com.elter.gameandroid.request.entity.ServerToken;
import com.elter.gameandroid.request.entity.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by elter on 07.10.17.
 */

public interface GameService {
    @GET("/")
    Call<ServerToken> getServerToken();

    @FormUrlEncoded
    @POST("/user/login")
    Call<Response> login(@Field("token") String token);

    @POST("/user/new")
    Call<Response> createNewUser(@Body User user);
}
