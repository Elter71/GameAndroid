package com.elter.gameandroid.request.entity;

import android.util.Base64;

import com.google.gson.annotations.SerializedName;

/**
 * Created by elter on 08.10.17.
 */

public class User {

    int number;
    String password;
    @SerializedName("character_name")
    String characterName;
    @SerializedName("token")
    String token;

    public User(int number, String password) {
        this.number = number;
        this.password = password;
    }

    public User(int number, String password, String characterName) {
        this.number = number;
        this.password = password;
        this.characterName = characterName;
    }

    public String getToken() {
        String data = number + ":" + password;
        return Base64.encodeToString(data.getBytes(), 0);
    }

    public void setToken(String token) {
        this.token = token;
    }
}
