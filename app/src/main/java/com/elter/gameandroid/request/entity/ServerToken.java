package com.elter.gameandroid.request.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by elter on 07.10.17.
 */

public class ServerToken {
    @SerializedName("token")
    String token;

    public ServerToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
