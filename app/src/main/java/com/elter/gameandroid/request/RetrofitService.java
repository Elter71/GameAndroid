package com.elter.gameandroid.request;

import com.elter.gameandroid.Settings;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by elter on 07.10.17.
 */

public class RetrofitService {
    private static RetrofitService instance;
    private GameService service;

    private RetrofitService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Settings.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(GameService.class);
    }

    public static RetrofitService getInstance() {
        if (instance == null) {
            instance = new RetrofitService();
        }
        return instance;
    }

    public GameService getService() {
        return service;
    }
}
