package com.elter.gameandroid.ui;

/**
 * Created by elter on 08.10.17.
 */

public interface TextInputActionEvent {
    void onActionEvent();
}
