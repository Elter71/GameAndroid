package com.elter.gameandroid.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.elter.gameandroid.R;

/**
 * Created by elter on 10.10.17.
 */

public class StatisticsView extends View {

    private Paint circlePaint;
    private Paint textPaint;
    private int circleWidth;
    private int circleHeight;
    private int textWidth;
    private int textHeight;
    private int radius;
    private String statistics;
    private boolean update;

    public StatisticsView(Context context) {
        super(context);
    }

    public StatisticsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        circlePaint = new Paint();
        textPaint = new Paint();
        circlePaint.setStyle(Paint.Style.FILL);
        textPaint.setStyle(Paint.Style.FILL);
        circlePaint.setColor(getResources().getColor(R.color.colorPrimary));
        textPaint.setColor(getResources().getColor(R.color.colorText));
        statistics = "0";
        update = false;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        calculateDrawElementSizeAndPosition();
        if (update) {
            statistics = "+";
        }
        setTextPaintSize();
        canvas.drawCircle(circleWidth, circleHeight, radius, circlePaint);
        canvas.drawText(statistics, textWidth, textHeight, textPaint);
    }


    private void calculateDrawElementSizeAndPosition() {
        circleWidth = getWidth() / 2;
        circleHeight = getHeight() / 2;
        radius = getWidth() / 5;
        if (statistics.length() >= 2) {
            textWidth = circleWidth - radius / 2;
        } else {
            textWidth = circleWidth - radius / 4;
        }

        textHeight = circleHeight + radius / 3;

    }

    private void setTextPaintSize() {
        textPaint.setTextSize(radius);
    }

    public void setStatistics(int statistics) {
        this.statistics = "" + statistics;
    }

    public void isUpdate(boolean update) {
        this.update = update;
    }

}
