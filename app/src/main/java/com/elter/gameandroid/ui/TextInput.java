package com.elter.gameandroid.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elter.gameandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by elter on 07.10.17.
 */

public class TextInput extends LinearLayout {

    @BindView(R.id.icon)
    ImageView imageView;

    @BindView(R.id.text)
    EditText text;

    public TextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.text_input, this);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextInput, 0, 0);
        ButterKnife.bind(this);
        imageView.setImageResource(typedArray.getResourceId(R.styleable.TextInput_image, 0));
        text.setHint(typedArray.getString(R.styleable.TextInput_hint));
        text.setInputType(typedArray.getInteger(R.styleable.TextInput_android_inputType, 0));

    }
    public EditText getEditText(){
        return text;
    }
    public void setActionEventLisner(final TextInputActionEvent textInputActionEvent){
        text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                textInputActionEvent.onActionEvent();
                return false;
            }
        });
    }
}
