package com.elter.gameandroid.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.elter.gameandroid.R;

/**
 * Created by elter on 09.10.17.
 */

public class CharacterView extends android.support.v7.widget.AppCompatImageView {
    private Paint circlePaint;
    private Paint textPaint;
    private int circleWidth;
    private int circleHeight;
    private int textWidth;
    private int textHeight;
    private int radius;
    private String level;

    public CharacterView(Context context) {
        super(context);
    }

    public CharacterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        circlePaint = new Paint();
        textPaint = new Paint();
        circlePaint.setStyle(Paint.Style.FILL);
        textPaint.setStyle(Paint.Style.FILL);
        circlePaint.setColor(getResources().getColor(R.color.colorPrimary));
        textPaint.setColor(getResources().getColor(R.color.colorText));
        level = "0";
    }

    public CharacterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        calculateDrawElementSizeAndPosition();
        setTextPaintSize();
        canvas.drawCircle(circleWidth, circleHeight, radius, circlePaint);
        canvas.drawText(level, textWidth, textHeight, textPaint);
    }

    private void calculateDrawElementSizeAndPosition() {
        circleWidth = (int) (getWidth() / 1.20);
        circleHeight = (int) (getHeight() / 1.14);
        radius = getWidth() / 12;
        if (level.length() >= 2) {
            textWidth = circleWidth - radius / 2;
        } else {
            textWidth = circleWidth - radius / 4;
        }
        textHeight = circleHeight + radius / 3;

    }

    private void setTextPaintSize() {
        textPaint.setTextSize(radius);
    }

    public void setLevel(int level) {
        this.level = "" + level;
    }
}
