package com.elter.gameandroid.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.elter.gameandroid.R;

/**
 * Created by elter on 10.10.17.
 */

public class CharacterFightView extends android.support.v7.widget.AppCompatImageView {
    private Paint circlePaint;
    private int circleWidth;
    private int circleHeight;
    private float textWidth;
    private float textHeight;
    private float radius;
    private String level;

    public CharacterFightView(Context context) {
        super(context);
    }

    public CharacterFightView(Context context, AttributeSet attrs) {
        super(context, attrs);
        circlePaint = new Paint();
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeWidth(10);
        circlePaint.setColor(getResources().getColor(R.color.colorPrimary));
        level = "0";
    }

    public CharacterFightView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        calculateDrawElementSizeAndPosition();
//        canvas.drawCircle(circleWidth, circleHeight, radius, circlePaint);

        Path path = new Path();
        path.addCircle(getWidth() / 2,
                getHeight() / 2, radius,
                Path.Direction.CW);

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.FILL);

        float center_x, center_y;
        final RectF oval = new RectF();
        paint.setStyle(Paint.Style.STROKE);

        center_x = getWidth() / 2;
        center_y = getHeight() / 2;

        oval.set(center_x - radius,
                center_y - radius,
                center_x + radius,
                center_y + radius);
        canvas.drawArc(oval, 0, 180,false, paint);
    }

    private void calculateDrawElementSizeAndPosition() {
        circleWidth = getWidth() / 2;
        circleHeight = (getHeight() / 2);
        radius = getWidth() / 2.2f;

    }

}