package com.elter.gameandroid.ui;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.elter.gameandroid.R;


/**
 * Created by elter on 07.10.17.
 */

public class ErrorDialog {
    private AlertDialog dialog;
    private View view;

    public ErrorDialog(String message, Activity activity) {
        view = getDialogLayout(activity);
        setMessageToDialogText(view, message);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(view);
        dialog = builder.create();
    }

    private void setMessageToDialogText(View view, String message) {
        TextView text = view.findViewById(R.id.dialog_text);
        text.setText(message);
    }

    private View getDialogLayout(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();
        return inflater.inflate(R.layout.error_dialog, null);
    }

    public void setOnExitButtonClickListner(View.OnClickListener listner) {
        Button button = view.findViewById(R.id.exit_button);
        button.setOnClickListener(listner);
    }

    public void setOnAgainButtonClickListner(View.OnClickListener listner) {
        Button button = view.findViewById(R.id.try_again_button);
        button.setOnClickListener(listner);
    }

    public void show() {
        dialog.show();
    }

    public void hide() {
        dialog.dismiss();
    }


}

