package com.elter.gameandroid.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by elter on 08.10.17.
 */

public class LogData {
    private SharedPreferences sharedPreferences;

    public LogData(Context context) {
        sharedPreferences = context.getSharedPreferences("USER_TOKEN", Context.MODE_PRIVATE);
    }

    public void saveData(String data) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("USER_TOKEN_DATA", data);
        editor.commit();
    }

    public String getData() {
        return sharedPreferences.getString("USER_TOKEN_DATA", null);
    }
}
