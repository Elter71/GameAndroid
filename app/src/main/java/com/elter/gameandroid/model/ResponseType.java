package com.elter.gameandroid.model;

import com.elter.gameandroid.R;

/**
 * Created by elter on 08.10.17.
 */

public class ResponseType {
    public static int getResponseMessage(int responseType) {
        switch (responseType) {
            case 2:
                return R.string.wrong_authorization;
            case 3:
                return R.string.user_already_exist;
            case 4:
                return R.string.character_name_already_exist;

            default:
                return R.string.wrong_connection_to_server_message;
        }
    }

}
