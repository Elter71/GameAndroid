package com.elter.gameandroid.screen.sing_up.presenter;

import com.elter.gameandroid.R;
import com.elter.gameandroid.model.ResponseType;
import com.elter.gameandroid.request.entity.Response;
import com.elter.gameandroid.request.entity.ServerToken;
import com.elter.gameandroid.request.entity.User;
import com.elter.gameandroid.screen.sing_up.interactor.SingUpInteractor;
import com.elter.gameandroid.screen.sing_up.router.SingUpRouter;
import com.elter.gameandroid.screen.sing_up.view.SingUpView;
import com.elter.gameandroid.ui.TextInput;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by elter on 06.10.17.
 */

public class SingUpPresenter {
    private SingUpRouter router;
    private SingUpView view;
    private SingUpInteractor interactor;
    private String userToken;

    public SingUpPresenter(SingUpView view, SingUpRouter router) {
        this.view = view;
        this.router = router;
        this.interactor = new SingUpInteractor();
    }

    public void createNewAccount(TextInput number, TextInput password, TextInput confirmPassword, TextInput characterName) {
        if (checkParameterCorrection(number, password, confirmPassword, characterName)) {
            User user = createUser(number, password, characterName);
            interactor.getServerToken(serverTokenCallback(user));
        }
    }

    private boolean checkParameterCorrection(TextInput number, TextInput password, TextInput confirmPassword, TextInput characterName) {
        if (number.getEditText().getText().length() < 9 || number.getEditText().getText().length() > 9) {
            view.shakeTextInput(number);
            view.showErrorToast(R.string.incorrect_number);
            return false;
        }
        if (password.getEditText().getText().length() < 7) {
            view.shakeTextInput(password);
            view.showErrorToast(R.string.incorrect_password_size);
            return false;
        }
        if (!password.getEditText().getText().toString().equals(confirmPassword.getEditText().getText().toString())) {
            view.shakeTextInput(confirmPassword);
            view.showErrorToast(R.string.incorrect_password);
            return false;
        }
        if (characterName.getEditText().getText().length() < 3 || characterName.getEditText().getText().length() > 10) {
            view.shakeTextInput(characterName);
            view.showErrorToast(R.string.incorrect_character);
            return false;
        }
        return true;
    }

    private User createUser(TextInput number, TextInput password, TextInput characterName) {
        String Number = number.getEditText().getText().toString();
        String Password = password.getEditText().getText().toString();
        String CharacterName = characterName.getEditText().getText().toString();
        User user = new User(Integer.parseInt(Number), Password, CharacterName);
        userToken = user.getToken();
        return user;
    }

    private Callback<ServerToken> serverTokenCallback(final User user) {
        return new Callback<ServerToken>() {
            @Override
            public void onResponse(Call<ServerToken> call, retrofit2.Response<ServerToken> response) {
                createUserRequest(user, response.body().getToken());
            }

            @Override
            public void onFailure(Call<ServerToken> call, Throwable t) {
                view.showWrongConnectionToast();
            }
        };
    }

    private void createUserRequest(User user, String serverToken) {
        user.setToken(serverToken);
        interactor.createUser(user, createUserCallback());
    }

    private Callback<Response> createUserCallback() {
        return new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                checkResponse(response.body());
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.showWrongConnectionToast();
            }
        };
    }

    private void checkResponse(Response response) {
        if (response.getType() == 99) {
            view.showSuccessToast();
            view.saveUserToken(userToken);
            router.navigateToMainActivity();
        } else {
            int message = ResponseType.getResponseMessage(response.getType());
            view.showErrorToast(message);
        }

    }


}
