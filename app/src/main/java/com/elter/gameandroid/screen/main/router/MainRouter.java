package com.elter.gameandroid.screen.main.router;

/**
 * Created by elter on 06.10.17.
 */

public interface MainRouter {
    void navigateToFightActivity();
}
