package com.elter.gameandroid.screen.login_in.view;

/**
 * Created by elter on 06.10.17.
 */

public interface LoginInView {
    void showWrongParamToast();
    void showWrongConnectionToast();
    void saveUserToken(String token);
}
