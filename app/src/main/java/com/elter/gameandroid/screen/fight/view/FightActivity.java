package com.elter.gameandroid.screen.fight.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.elter.gameandroid.R;
import com.elter.gameandroid.screen.fight.presenter.FightPresenter;
import com.elter.gameandroid.screen.fight.router.FightRouter;

import butterknife.ButterKnife;


public class FightActivity extends AppCompatActivity implements FightView, FightRouter {
    private FightPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fight_activity);
        ButterKnife.bind(this);
        presenter = new FightPresenter(this, this);
    }
    @Override
    public void onBackPressed() {

    }

}
