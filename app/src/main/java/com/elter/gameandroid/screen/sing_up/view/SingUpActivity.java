package com.elter.gameandroid.screen.sing_up.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.elter.gameandroid.R;
import com.elter.gameandroid.model.LogData;
import com.elter.gameandroid.screen.main.view.MainActivity;
import com.elter.gameandroid.screen.sing_up.presenter.SingUpPresenter;
import com.elter.gameandroid.screen.sing_up.router.SingUpRouter;
import com.elter.gameandroid.ui.TextInput;
import com.elter.gameandroid.ui.TextInputActionEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class SingUpActivity extends AppCompatActivity implements SingUpView, SingUpRouter, TextInputActionEvent {
    private SingUpPresenter presenter;
    @BindView(R.id.Title)
    TextView title;
    private Animation shake;

    @BindView(R.id.number_text)
    TextInput number;
    @BindView(R.id.password_text)
    TextInput password;
    @BindView(R.id.confirm_password_text)
    TextInput confirmPassword;
    @BindView(R.id.character_name_text)
    TextInput characterName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sing_up_activity);
        ButterKnife.bind(this);
        title.setText(R.string.sing_up_title);
        presenter = new SingUpPresenter(this, this);
        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.animator.shake);

    }

    @OnClick(R.id.singUp)
    public void onSingUpClick() {
        presenter.createNewAccount(number, password, confirmPassword, characterName);
    }

    @Override
    public void saveUserToken(String token) {
        LogData data = new LogData(this);
        data.saveData(token);
    }

    @Override
    public void showErrorToast(int message) {
        Toasty.error(this, getString(message)).show();
    }

    @Override
    public void shakeTextInput(TextInput textInput) {
        textInput.startAnimation(shake);
    }

    @Override
    public void showWrongConnectionToast() {
        Toasty.info(this, getString(R.string.wrong_connection_to_server_message)).show();
    }

    @Override
    public void showSuccessToast() {
        Toasty.success(this, "Success create account").show();
    }

    @Override
    public void onActionEvent() {
        presenter.createNewAccount(number, password, confirmPassword, characterName);
    }

    @Override
    public void navigateToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
