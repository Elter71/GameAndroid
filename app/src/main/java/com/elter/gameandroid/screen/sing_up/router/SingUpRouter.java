package com.elter.gameandroid.screen.sing_up.router;

/**
 * Created by elter on 06.10.17.
 */

public interface SingUpRouter {
    void navigateToMainActivity();
}
