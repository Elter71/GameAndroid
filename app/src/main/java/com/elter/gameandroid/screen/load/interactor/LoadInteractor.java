package com.elter.gameandroid.screen.load.interactor;

import com.elter.gameandroid.Interactor;
import com.elter.gameandroid.request.entity.ServerToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elter on 06.10.17.
 */

public class LoadInteractor extends Interactor {
    public Call<ServerToken> serverConnection() {
        return this.service.getServerToken();
    }
}
