package com.elter.gameandroid.screen.login_in.router;

/**
 * Created by elter on 06.10.17.
 */

public interface LoginInRouter {
    void navigateToSingUpActivity();
    void navigateToMainActivity();
}
