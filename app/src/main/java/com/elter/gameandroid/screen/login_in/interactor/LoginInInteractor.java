package com.elter.gameandroid.screen.login_in.interactor;

import com.elter.gameandroid.Interactor;
import com.elter.gameandroid.request.entity.Response;
import retrofit2.Callback;

/**
 * Created by elter on 06.10.17.
 */

public class LoginInInteractor extends Interactor {
    public void loginRequest(String token, Callback<Response> callback) {
        service.login(token).enqueue(callback);
    }
}
