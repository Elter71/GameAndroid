package com.elter.gameandroid.screen.main.presenter;

import com.elter.gameandroid.request.entity.Response;
import com.elter.gameandroid.request.entity.User;
import com.elter.gameandroid.screen.main.interactor.MainInteractor;
import com.elter.gameandroid.screen.main.router.MainRouter;
import com.elter.gameandroid.screen.main.view.MainView;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by elter on 06.10.17.
 */

public class MainPresenter {
    private MainRouter router;
    private MainView view;
    private MainInteractor interactor;

    public MainPresenter(MainView view, MainRouter router) {
        this.view = view;
        this.router = router;
        this.interactor = new MainInteractor();
    }


    public void startFight() {
        router.navigateToFightActivity();
    }

    public void exitApp() {
        view.exit();
    }
}

