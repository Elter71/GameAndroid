package com.elter.gameandroid.screen.load.view;

/**
 * Created by elter on 06.10.17.
 */

public interface LoadView {
    void startProgressBar();
    void stopProgressBar();
    void hideErrorDialog();
    void showErrorDialog();
    void exitApplication();
}
