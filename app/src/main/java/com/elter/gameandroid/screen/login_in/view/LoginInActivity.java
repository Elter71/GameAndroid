package com.elter.gameandroid.screen.login_in.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.elter.gameandroid.R;
import com.elter.gameandroid.model.LogData;
import com.elter.gameandroid.screen.login_in.presenter.LoginInPresenter;
import com.elter.gameandroid.screen.login_in.router.LoginInRouter;
import com.elter.gameandroid.screen.main.view.MainActivity;
import com.elter.gameandroid.screen.sing_up.view.SingUpActivity;
import com.elter.gameandroid.ui.TextInput;
import com.elter.gameandroid.ui.TextInputActionEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class LoginInActivity extends AppCompatActivity implements LoginInView, LoginInRouter, TextInputActionEvent {
    private LoginInPresenter presenter;
    @BindView(R.id.Title)
    TextView title;

    @BindView(R.id.password_text)
    TextInput password;
    @BindView(R.id.number_text)
    TextInput number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_in_activity);
        ButterKnife.bind(this);
        title.setText(R.string.login_in_title);
        presenter = new LoginInPresenter(this, this);
        password.setActionEventLisner(this);
    }

    @OnClick(R.id.singUp)
    public void onSingUpClick() {
        presenter.changeActivityToSingUp();
    }


    @Override
    public void navigateToSingUpActivity() {
        Intent intent = new Intent(this, SingUpActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showWrongParamToast() {
        Toasty.error(this, getString(R.string.wrong_authorization)).show();
    }

    @Override
    public void showWrongConnectionToast() {
        Toasty.info(this, getString(R.string.wrong_connection_to_server_message)).show();
    }

    @Override
    public void saveUserToken(String token) {
        LogData data = new LogData(this);
        data.saveData(token);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onActionEvent() {
        presenter.loginIn(number.getEditText().getText().toString(), password.getEditText().getText().toString());
    }
}
