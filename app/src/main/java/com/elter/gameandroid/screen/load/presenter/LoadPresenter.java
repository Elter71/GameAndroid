package com.elter.gameandroid.screen.load.presenter;

import com.elter.gameandroid.request.entity.ServerToken;
import com.elter.gameandroid.screen.load.interactor.LoadInteractor;
import com.elter.gameandroid.screen.load.router.LoadRouter;
import com.elter.gameandroid.screen.load.view.LoadView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elter on 06.10.17.
 */

public class LoadPresenter {
    private LoadRouter router;
    private LoadView view;
    private LoadInteractor interactor;

    public LoadPresenter(LoadView view, LoadRouter router) {
        this.view = view;
        this.router = router;
        this.interactor = new LoadInteractor();
    }

    public void checkServerConnection(final String userToken) {
        view.hideErrorDialog();
        view.startProgressBar();
        interactor.serverConnection().enqueue(new Callback<ServerToken>() {
            @Override
            public void onResponse(Call<ServerToken> call, Response<ServerToken> response) {
                view.stopProgressBar();
                checkUserToken(userToken);
            }

            @Override
            public void onFailure(Call<ServerToken> call, Throwable t) {
                view.showErrorDialog();
            }
        });
    }

    private void checkUserToken(String userToken) {
        if (userToken == null) {
            router.navigateToLoginInActivity();
        } else {
            router.navigateToMainActivity();
        }
    }

    public void exitApplication() {
        view.hideErrorDialog();
        view.exitApplication();
    }
}
