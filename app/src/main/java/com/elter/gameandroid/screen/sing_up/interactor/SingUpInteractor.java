package com.elter.gameandroid.screen.sing_up.interactor;

import com.elter.gameandroid.Interactor;
import com.elter.gameandroid.request.entity.Response;
import com.elter.gameandroid.request.entity.ServerToken;
import com.elter.gameandroid.request.entity.User;

import retrofit2.Callback;

/**
 * Created by elter on 06.10.17.
 */

public class SingUpInteractor extends Interactor {
    public void createUser(User user, Callback<Response> callback) {
        service.createNewUser(user).enqueue(callback);
    }

    public void getServerToken(Callback<ServerToken> callback) {
        service.getServerToken().enqueue(callback);
    }
}
