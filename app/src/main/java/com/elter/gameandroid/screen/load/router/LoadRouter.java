package com.elter.gameandroid.screen.load.router;

/**
 * Created by elter on 06.10.17.
 */

public interface LoadRouter {
    void navigateToLoginInActivity();
    void navigateToMainActivity();
}
