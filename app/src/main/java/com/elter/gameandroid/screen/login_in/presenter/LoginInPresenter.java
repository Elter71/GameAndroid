package com.elter.gameandroid.screen.login_in.presenter;

import com.elter.gameandroid.request.entity.Response;
import com.elter.gameandroid.request.entity.User;
import com.elter.gameandroid.screen.login_in.interactor.LoginInInteractor;
import com.elter.gameandroid.screen.login_in.router.LoginInRouter;
import com.elter.gameandroid.screen.login_in.view.LoginInView;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by elter on 06.10.17.
 */

public class LoginInPresenter {
    private LoginInRouter router;
    private LoginInView view;
    private LoginInInteractor interactor;

    private String userToken;

    public LoginInPresenter(LoginInView view, LoginInRouter router) {
        this.view = view;
        this.router = router;
        this.interactor = new LoginInInteractor();

    }

    public void changeActivityToSingUp() {
        router.navigateToSingUpActivity();
    }

    public void loginIn(String number, String password) {
        if (!number.isEmpty() && !password.isEmpty()) {
            User user = new User(Integer.parseInt(number), password);
            userToken = user.getToken();
            interactor.loginRequest(userToken, loginInCallback());
        }
    }

    private Callback<Response> loginInCallback() {
        return new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                checkLoginInResponse(response.body());
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.showWrongConnectionToast();
            }
        };
    }

    private void checkLoginInResponse(Response response) {
        if (response.getType() == 99) {
            view.saveUserToken(userToken);
            router.navigateToMainActivity();
        } else {
            view.showWrongParamToast();
        }
    }


}

