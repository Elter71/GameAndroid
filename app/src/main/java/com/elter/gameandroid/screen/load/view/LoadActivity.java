package com.elter.gameandroid.screen.load.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.elter.gameandroid.R;
import com.elter.gameandroid.model.LogData;
import com.elter.gameandroid.screen.load.presenter.LoadPresenter;
import com.elter.gameandroid.screen.load.router.LoadRouter;
import com.elter.gameandroid.screen.login_in.view.LoginInActivity;
import com.elter.gameandroid.screen.main.view.MainActivity;
import com.elter.gameandroid.ui.ErrorDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoadActivity extends AppCompatActivity implements LoadView, LoadRouter {
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private LoadPresenter presenter;
    private ErrorDialog errorDialog;
    private String userToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_activity);
        ButterKnife.bind(this);
        createErrorDialog();
        presenter = new LoadPresenter(this, this);
        LogData data = new LogData(this);
        userToken = data.getData();
        presenter.checkServerConnection(userToken);
    }

    private void createErrorDialog() {
        errorDialog = new ErrorDialog(getString(R.string.wrong_connection_to_server_message), this);
        errorDialog.setOnAgainButtonClickListner(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.checkServerConnection(userToken);
            }
        });
        errorDialog.setOnExitButtonClickListner(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.exitApplication();
            }
        });

    }

    @Override
    public void startProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorDialog() {
        errorDialog.show();
    }

    @Override
    public void hideErrorDialog() {
        errorDialog.hide();
    }

    @Override
    public void exitApplication() {
        this.finish();
    }

    @Override
    public void navigateToLoginInActivity() {
        Intent intent = new Intent(this, LoginInActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
