package com.elter.gameandroid.screen.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.elter.gameandroid.R;
import com.elter.gameandroid.screen.fight.view.FightActivity;
import com.elter.gameandroid.screen.main.presenter.MainPresenter;
import com.elter.gameandroid.screen.main.router.MainRouter;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity implements MainView, MainRouter {
    private MainPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this, this);
    }

    @OnClick(R.id.imageFight)
    public void onFightClick() {
        presenter.startFight();
    }

    @OnClick(R.id.imageExit)
    public void onExitClick() {
        presenter.exitApp();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void navigateToFightActivity() {
        Intent intent = new Intent(this, FightActivity.class);
        startActivity(intent);
    }

    @Override
    public void exit() {
        finishAffinity();
    }
}
