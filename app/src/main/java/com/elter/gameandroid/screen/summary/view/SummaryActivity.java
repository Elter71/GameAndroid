package com.elter.gameandroid.screen.summary.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.elter.gameandroid.R;
import com.elter.gameandroid.screen.summary.presenter.SummaryPresenter;
import com.elter.gameandroid.screen.summary.router.SummaryRouter;

import butterknife.ButterKnife;


public class SummaryActivity extends AppCompatActivity implements SummaryView, SummaryRouter {
    private SummaryPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        presenter = new SummaryPresenter(this, this);
    }
    @Override
    public void onBackPressed() {



    }

}
