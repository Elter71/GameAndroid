package com.elter.gameandroid.screen.summary.presenter;

import com.elter.gameandroid.screen.summary.interactor.SummaryInteractor;
import com.elter.gameandroid.screen.summary.router.SummaryRouter;
import com.elter.gameandroid.screen.summary.view.SummaryView;


/**
 * Created by elter on 06.10.17.
 */

public class SummaryPresenter {
    private SummaryRouter router;
    private SummaryView view;
    private SummaryInteractor interactor;

    public SummaryPresenter(SummaryView view, SummaryRouter router) {
        this.view = view;
        this.router = router;
        this.interactor = new SummaryInteractor();
    }



}

