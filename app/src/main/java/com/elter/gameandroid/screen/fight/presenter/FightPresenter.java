package com.elter.gameandroid.screen.fight.presenter;

import com.elter.gameandroid.screen.fight.interactor.FightInteractor;
import com.elter.gameandroid.screen.fight.router.FightRouter;
import com.elter.gameandroid.screen.fight.view.FightView;


/**
 * Created by elter on 06.10.17.
 */

public class FightPresenter {
    private FightRouter router;
    private FightView view;
    private FightInteractor interactor;

    public FightPresenter(FightView view, FightRouter router) {
        this.view = view;
        this.router = router;
        this.interactor = new FightInteractor();
    }



}

