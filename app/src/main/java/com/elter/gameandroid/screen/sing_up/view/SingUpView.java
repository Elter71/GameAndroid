package com.elter.gameandroid.screen.sing_up.view;

import com.elter.gameandroid.ui.TextInput;

/**
 * Created by elter on 06.10.17.
 */

public interface SingUpView {
    void showErrorToast(int message);
    void shakeTextInput(TextInput textInput);
    void showWrongConnectionToast();
    void showSuccessToast();
    void saveUserToken(String token);
}
